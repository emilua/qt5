#!/usr/bin/env -S bash --

TEST="$1"

exec diff -u "$TEST.out" <("$EMILUA_BIN" "$TEST.lua")
