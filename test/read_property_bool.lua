local qt = require 'qt5'

local qml = qt.load_qml(byte_span.append([[
    import QtQml 2.0

    QtObject {
        id: attributes
        property bool my_property: true
    }
]]))

print(qml.property.my_property)
print(qml.object.my_property)
print(qml.property.attributes.my_property)
