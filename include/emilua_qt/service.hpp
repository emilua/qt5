// Copyright (c) 2023 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#pragma once

#include <boost/asio/execution_context.hpp>
#include <emilua_qt/qt_handle.hpp>

namespace emilua_qt {

// This service is only used to keep the Qt event loop alive for as long as Lua
// VMs running on it exist. It's more convenient to just attach it to
// asio::io_context than to attach it to each Lua VM individually.
//
// Furthermore, the Qt event loop still needs to exist until events posted from
// the Lua VMs are processed which will happen upon VM destruction as well. So
// tying its lifetime to asio::io_context instead of closures that are destroyed
// upon shutdown() (the Lua VMs) makes the overall design cleaner and easier to
// handle.
class service : public boost::asio::execution_context::service
{
public:
    using key_type = service;

    explicit service(boost::asio::execution_context& ctx,
                     std::shared_ptr<qt_handle> handle);

    static boost::asio::io_context::id id;

private:
    void shutdown() noexcept override;

    std::shared_ptr<qt_handle> handle;
};

} // namespace emilua_qt
