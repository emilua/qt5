// Copyright (c) 2023 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#pragma once

#include <QtCore/QObject>
#include <memory>

namespace emilua_qt {

class ByteSpanResource : public QObject
{
    Q_OBJECT
public:
    explicit ByteSpanResource(QObject* parent = nullptr);

    std::shared_ptr<unsigned char[]> data;
};

} // namespace emilua_qt
